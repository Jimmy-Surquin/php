<!doctype html>
<html>
<head>
  <title>Joueurs</title>
  <meta charset='utf-8'>
  <link rel="stylesheet" href="style.css">
</head>
<body>
  <div class="joueurs">
    <form action="create_nom.php" method="GET">
      <label for="nom">Nouveau Joueur</label>
      <input name="nom" type="text" placeholder="Nouveau Joueur" tabindex="1">
      <input type="submit">
    </form>
    <ul>
      <?php
      ini_set("display_errors","true");
      $handle=mysqli_connect ("localhost","root","1234","PenduApp");
      $query="SELECT * FROM joueurs";
      $result=mysqli_query ($handle,$query);
      while($line=mysqli_fetch_array($result)) {
        echo "\t<li>".$line["id"]."&nbsp".$line["nom"];
        echo "&nbsp;<a href=\"delete_nom.php?id=".$line["id"]."\">X</a>";
        echo "&nbsp;<a href=\"input_nom.php?id=". $line ["id"] . "\">UPDATE</a>";
        echo "</li>\n";
      }
      ?>
    </ul>
  </div>
  <div class="mots">
    <form action="create_mot.php" method="GET">
      <label for="mot">Nouveau mot</label>
      <input name="mot" type="text" placeholder="Nouveau mot" tabindex="1">
      <input type="submit">
    </form>
    <ul>
      <?php
      ini_set("display_errors","true");
      $handle=mysqli_connect ("localhost","root","1234","PenduApp");
      $query="SELECT * FROM mots";
      $result=mysqli_query ($handle,$query);
      while($line=mysqli_fetch_array($result)) {
        echo "\t<li>".$line["id"]."&nbsp".$line["mot"];
        echo "&nbsp;<a href=\"delete_mot.php?id=".$line["id"]."\">X</a>";
        echo "&nbsp;<a href=\"input_mot.php?id=". $line ["id"] . "\">UPDATE</a>";
        echo "</li>\n";

      }
      ?>
    </ul>
  </div>
</body>
</html>
