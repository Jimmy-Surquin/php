<!doctype html>
<html>
<head>
	<title>Modification d'un joueur</title>
	<meta charset>
	<link rel="stylesheet" type="text/css" href="stylependu.css">
</head>
<body>
      <form action="update_nom.php" method="GET">
         <label for="nom">Modifier nom du joueur par :</label>
         <input name="nom" type="text" placeholder="Votre nouveau nom" tabindex="1" autocomplete="off">
         <input type="hidden" name="id" value="<?php echo $_GET["id"]; ?>" >
         <input type="submit">
      </form>
   <br>
<a href="joueurs.php">Retour</a>
</body>
</html>
