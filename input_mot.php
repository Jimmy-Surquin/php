<!doctype html>
<html>
<head>
	<title>Modification d'un mot</title>
	<meta charset>
	<link rel="stylesheet" type="text/css" href="stylependu.css">
</head>
<body>
	<form action="update_mot.php" method="GET">
		<label for="mot">Modifier mot par :</label>
		<input name="mot" type="text" placeholder="Votre nouveau mot" tabindex="1" autocomplete="off">
		<input type="hidden" name="id" value="<?php echo $_GET["id"]; ?>" >
		<input type="submit">
	</form>
	<br>
	<a href="mots.php">Retour</a>
</body>
</html>
