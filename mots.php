<!doctype html>
<html>
<head>
  <title>Mots</title>
  <meta charset='utf-8'>
  <link rel="stylesheet" href="stylependu.css">
</head>
<body>
  <div class="mots">
    <div class="base">
      <form action="create_mot.php" method="GET">
        <input name="mot" type="text" placeholder="Nouveau mot" tabindex="1">
        <input type="submit">
      </form>
      <ul>
        <?php
        ini_set("display_errors","true");
        $handle=mysqli_connect ("localhost","root","1234","PenduApp");
        $query="SELECT * FROM mots";
        $result=mysqli_query ($handle,$query);
        while($line=mysqli_fetch_array($result)) {
          echo "\t<li>".$line["id"]."&nbsp;".$line["mot"];
          echo "<br>";
          echo "&nbsp;<a href=\"delete_mots.php?id=".$line["id"]."\">X</a>";
          echo "&nbsp;<a href=\"input_mot.php?id=". $line ["id"] . "\">UPDATE</a>";
          echo "</li>\n";
        }
        ?>
      </ul>
      <a href="index.php"> <span> Retour </span> </a>
    </div>
  </div>
</body>
</html>
